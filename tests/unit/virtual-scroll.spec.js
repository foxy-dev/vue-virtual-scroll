import VVirtualScroll from '@/components/v-virtual-scroll';
import { shallowMount, } from '@vue/test-utils';

describe('v-virtual-scroll.js', () => {
    let mountFunction;
    let propsData;
    let mock;
    const elementHeight = 100;
    const generateData = n => new Array(n).fill(null).map((el, i) => i);
    const createProps = (n = 10, height = 50) => ({
        height: elementHeight,
        items: generateData(n),
        itemHeight: height,
    });

    beforeEach(() => {
        mountFunction = (options = {}) => {
            return shallowMount(
                VVirtualScroll,
                Object.assign(Object.assign({}, options), { scopedSlots: {
                    default({ item }) {
                        return this.$createElement('div', { class: 'item' }, item);
                    },
                } })
            );
        };

        propsData = createProps(3);

        // mock clientHeight
        mock = jest.spyOn(window.HTMLElement.prototype, 'clientHeight', 'get').mockReturnValue(elementHeight);
    });

    afterEach(() => mock.mockRestore());

    it('should render component with scopedSlot and match snapshot', async () => {
        const wrapper = mountFunction({ propsData });
        await wrapper.vm.$nextTick();

        expect(wrapper.html()).toMatchSnapshot();
    });

    it('should set height of scrollable element', () => {
        const wrapper = mountFunction({ propsData });
        const scrollable = wrapper.find('.v-virtual-scroll__container');

        expect(scrollable.element.style.height).toEqual('150px');
    });

    it('should render not more than 5 hidden items and match snapshot', () => {
        const wrapper = mountFunction({ propsData: createProps() });

        expect(wrapper.html()).toMatchSnapshot();
    });

    it('should render right items on scroll and match snapshot', () => {
        const wrapper = mountFunction({ propsData: createProps(50) });

        wrapper.vm.scrollTop = 500;
        wrapper.trigger('scroll');
        expect(wrapper.html()).toMatchSnapshot();
    });

    it('should provide the correct item index', () => {
        const helpers = require('../../src/utils/helpers');
        const spy = jest.spyOn(helpers, 'getSlot');

        const wrapper = mountFunction({
            propsData,
            computed: { firstToRender: () => 2 },
        });

        wrapper.setData({ first: 2 });
        wrapper.vm.genChild(0, 1);

        expect(spy.mock.calls[0][2]).toEqual({
            item: 0,
            index: 3,
        });
    });
});
