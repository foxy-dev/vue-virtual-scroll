# Vue-virtual-scroll

![vue2](https://img.shields.io/badge/vue-2.6.x-brightgreen.svg)
![free](https://img.shields.io/badge/open-source-red.svg)
![ver](https://img.shields.io/badge/ver-0.1.0-blue.svg)

The `v-virtual-scroll` component can render an unlimited amount of items by rendering only what it needs to fill the scroller’s viewport.

## Table of Contents
- **[Demo](#demo)**
- **[Installation](#install)**
- **[Usage](#usage)**
    - [Basic example](#basic-example)
    - [Props](#props)
    - [Slots](#slots)
- **[Important notes](#important-notes)**
- **[How does it work?](#how-does-it-work)**
- **[License](#license)**

## Demo

![Vue Virtual Scroll](docs/plugin.gif?raw=true "Vue Virtual Scroll")

> virtual scroller displays just enough records to fill the viewport and uses the existing component, rehydrating it with new data. 
> 
> **[Demo Link](https://foxy-dev.gitlab.io/vue-virtual-scroll/)**.

## Installation

This project uses `node` and `yarn`. Go check them out if you don't have them locally installed.

#### Project setup
```
$ yarn install
```

#### Compiles and hot-reloads for development
```
$ yarn serve
```

#### Compiles and minifies for production
```
$ yarn build
```

#### Run your unit tests
```
$ yarn test:unit
```

#### Lints and fixes files
```
$ yarn lint
```

---

## Usage

>`v-virtual-scroll` is a virtual scroller that only renders the visible items. 
>As the user scrolls, `v-virtual-scroll` reuses all components and DOM nodes to maintain optimal performance.

### Basic example

Use the scoped slot to render each item in the list:

```html
<!--App.vue-->
<template>
    <v-virtual-scroll
            :height="height"
            :items="items"
            :item-height="itemHeight"
            :threshold="threshold"
    >
        <template v-slot:default="{ item }">
            <span>item</span>
        </template>
    </v-virtual-scroll>
</template>

<script>
import VVirtualScroll from '@/components/v-virtual-scroll';

export default {
    components: { VVirtualScroll },
    data () {}
        return {
            items: new Array(10000)
                    .fill(null)
                    .map((item, index) => 'Item ' + (index + 1)),
            height: 400,
            itemHeight: 26,
            threshold: 0,
        },
    }
</script>
```

### Props

Name  |Required |      Type         | Default     | Description
:----|:----: |:----:|:----:|:----   
`:threshold` | `false` | `[Number, String]` | `0` | The number of items outside the user view that are rendered.
`:height` | `true` | `[Number, String]` | `undefined` | Height of the component as a css value.
`:item-height` | `true` | `[Number, String]` | `undefined` | Height in pixels of the items to display.
`:items` | `false` | `[Array]` | `[]` | The array of items to display.
`:max-height` | `false` | `[Number, String]` | `undefined` | Sets the maximum height for the component.
`:max-widht` | `false` | `[Number, String]` | `undefined` | Sets the maximum width for the component.
`:min-height` | `false` | `[Number, String]` | `undefined` | Sets the minimum height for the component.
`:min-widht` | `false` | `[Number, String]` | `undefined` | Sets the minimum width for the component.
`:widht` | `false` | `[Number, String]` | `undefined` | Sets the width for the component.
`:calc-dynamic` | `false` | `[Boolean]` | `false` | Turn on dynamic calculation of items height.
`:per-row` | `false` | `[Number, String]` | `1` | Sets the amount of components must be rendered per row. _`:calc-dynamic: true`_ required.

### Slots
Name  | Parameters |Description
:----|---| :----   
`default` | `item: any`, `index: number` | Default slot to customize items appearance.

## Important notes

- **⚠️ You need to set the size of the `v-virtual-scroll` element and the items elements.**
- It is not recommended to use functional components inside `v-virtual-scroll` since the components are reused (so it will actually be slower).
- You don't need to set `key` on list content (but you should on all nested `<img>` elements to prevent load glitches).
- The browsers have a size limitation on DOM elements, it means that currently the virtual scroller can't display more than ~10k items depending on the browser.
- By default the `v-virtual-scroll` does not pre-render additional items outside of the viewport. Using the `threshold` prop will have the scroller render additional items as padding.

---

## How does it work?

- The `v-virtual-scroll` creates pools of views to render visible items to the user.
- A view holds a rendered item, and is reused inside its pool.
- For each type of item, a new pool is created so that the same components (and DOM trees) are reused for the same type. 
- Views will be deactivated if they go off-screen, and can be reused anytime for a newly visible item.

## License

[![MIT](https://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT)

© Foxy Dev