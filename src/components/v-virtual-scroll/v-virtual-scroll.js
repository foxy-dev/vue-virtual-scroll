import './v-virtual-scroll.scss';
import Scroll from '@/directives/scroll.js';
import { getSlot } from '@/utils/helpers';

/**
 * Create a virtual-scroller for big amount of items
 */
export default {
    name: 'v-virtual-scroll',

    directives: { Scroll },

    props: {
        /**
         * Height of the component as a css value.
         */
        height: {
            type: [ Number, String ],
            default: 200,
        },
        /**
         * Sets the width for the component.
         */
        width: [ Number, String ],
        /**
         * Sets the minimum height for the component.
         */
        minHeight: [ Number, String ],
        /**
         * Sets the minimum width for the component.
         */
        minWidth: [ Number, String ],
        /**
         * Sets the maximum height for the component.
         */
        maxHeight: [ Number, String ],
        /**
         * Sets the maximum width for the component.
         */
        maxWidth: [ Number, String ],
        /**
         * Height in pixels of the items to display
         */
        itemHeight: {
            type: [ Number, String ],
            required: true
        },
        /**
         * The array of items to display
         */
        items: {
            type: Array,
            default: () => []
        },
        /**
         * The number of items outside the user view that are rendered
         */
        threshold: {
            type: [ Number, String ],
            default: 0
        },
        /**
         * The number of items per row
         */
        perRow: {
            type: [ Number, String ],
            default: 1
        },

        calcDynamic: {
            type: Boolean,
            default: false,
        }
    },

    data() {
        return {
            first: 0,
            last: 0,
            scrollTop: 0,
            className: 'v-virtual-scroll',
            dynamicHeight: this.itemHeight,
        }
    },

    computed: {
        __threshold () {
            return parseInt(this.threshold, 10);
        },

        __itemHeight () {
            return this.calcDynamic
                ? this.dynamicHeight
                : parseInt(this.itemHeight, 10);
        },

        firstToRender () {
            return Math.max(0, this.first - this.__threshold);
        },

        lastToRender () {
            return Math.min(this.items.length, this.last + this.__threshold);
        },

        rootHeight () {
            return parseInt(this.height || 0, 10) || this.$el.clientHeight
        },

        measurableStyles () {
            return new Proxy(this.$props, {
                get: (obj, prop) =>
                    ['number', 'string'].includes(typeof obj[prop]) && this.toUnit(obj[prop]),
            });
        },
    },

    watch: {
        height: 'onScroll',
        itemHeight: 'onScroll',
        perRow: 'calcDynamicHeight',
    },

    mounted() {
        this.last = this.getLast(0);
    },

    methods: {
        toUnit (num, type = 'px') {
            return num + type;
        },

        genChild (item, index) {
            index += this.firstToRender;
            const row = this.countRow(index);

            return this.$createElement('div', {
                    staticClass: this.className + '__item',
                    style: {
                        top: this.toUnit(((index - row) * this.__itemHeight) / this.perRow ),
                        left: this.toUnit((100 / this.perRow) * row,'%'),
                        width: this.toUnit(100 / this.perRow, '%'),
                    },
                    key: index,
                },
                getSlot(this,'default', {index, item}),
            );
        },

        getChildren () {
            return this.items.slice(this.firstToRender, this.lastToRender * this.perRow).map(this.genChild);
        },

        getFirst () {
            return Math.floor(this.scrollTop / this.__itemHeight);
        },

        getLast (first) {
            return first + Math.ceil(this.rootHeight / this.__itemHeight);
        },

        countRow (index) {
            return (index % this.perRow).toFixed(0);
        },

        onScroll () {
            this.scrollTop = this.$el.scrollTop;
            this.first = this.getFirst();
            this.last = this.getLast(this.first);
        },

        calcDynamicHeight () {
            if ( !this.calcDynamic ) return;

            this.$nextTick(() => {
                const items = this.$el.querySelectorAll(`.${this.className + '__item'}`);
                this.dynamicHeight = [...items].reduce((height, curr) =>
                    Math.max(curr.getBoundingClientRect().height, height), this.itemHeight);
            });
        },
    },

    render (h) {
        const content = h('div', {
                staticClass: this.className + '__container',
                style: { height: this.toUnit(this.items.length * this.__itemHeight) },
            }, this.getChildren()
        );

        return h('div', {
                staticClass: this.className,
                style: this.measurableStyles,
                directives: [{ name: 'scroll', modifiers: { self: true }, value: this.onScroll }],
                on: this.$listeners,
            }, [content],
        );
    },
};
