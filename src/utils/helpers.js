export const getSlot = (vm, name = 'default', data, optional = false) => {
    if (vm.$scopedSlots[name])
        return vm.$scopedSlots[name](data instanceof Function ? data() : data);

    if (vm.$slots[name] && (!data || optional))
        return vm.$slots[name];

    return undefined;
};

export default { getSlot };