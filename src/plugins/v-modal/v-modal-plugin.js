import M from './components/v-modal-list';
import Modal from './components/EagleModal';
import ParentFile from './components/types/Parent';

M.install = (Vue, options) => {
    Vue.prototype.$modals = new (Vue.extend(M))({
        propsData: options
    })
    Vue.modals = Vue.prototype.$modals
}

export const EagleModal = M;
export const Parent = ParentFile;
export const eModal = Modal;